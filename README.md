# fourchette

Le but de ce dojo était de créer le jeu de la fourchette (un jeu ou le but est
de trouver un chiffre en ayant des indications plus grand / plus petit) dans une
page web, soit en HTML et JavaScript.

Le résultat de ce dojo se trouve dans la page [index.html](index.html).

## Autres versions
Si vous avez créé une autre version, vous pouvez l'ajouter ci-dessous:
  * [@ponsfrilus](ponsfrilus.html)
